package interpreter;

import java.util.Arrays;
import java.util.List;

public class Parser {
    private Lexer lexer;
    private Token currentToken;

    public Parser(Lexer lexer) throws Exception {
        this.lexer = lexer;
        currentToken = this.lexer.nextToken();
    }

    private void checkTokenType(TokenType type) throws Exception {
        if (currentToken.getType() == type){
            currentToken = lexer.nextToken();
        }
        else {
            throw new Exception("Parser error");
        }
    }

    private Node factor() throws Exception {
        Token token = currentToken;
        if (token.getType().equals(TokenType.PLUS)){
            checkTokenType(TokenType.PLUS);
            return new UnaryOp(token, factor());
        }
        else if (token.getType().equals(TokenType.MINUS)){
            checkTokenType(TokenType.MINUS);
            return new UnaryOp(token, factor());
        }
        else if (token.getType().equals(TokenType.INTEGER)) {
            checkTokenType(TokenType.INTEGER);
            return new Number(token);
        }
        else if (token.getType().equals(TokenType.LPAREN)) {
            checkTokenType(TokenType.LPAREN);
            Node node = expr();
            checkTokenType(TokenType.RPAREN);
            return node;
        }
        throw new Exception("Factor error");
    }

    private Node term() throws Exception {
        Node result = factor();
        List<TokenType> ops = Arrays.asList(TokenType.DIV, TokenType.MUL);
        while (ops.contains(currentToken.getType())){
            Token token = currentToken;
            if (token.getType() == TokenType.MUL){
                checkTokenType(TokenType.MUL);
            }
            else if(token.getType() == TokenType.DIV){
                checkTokenType(TokenType.DIV);
            }
            result = new BinOp(result, token, factor());
        }
        return result;
    }

    public Node expr() throws Exception {
        List<TokenType> ops = Arrays.asList(TokenType.PLUS, TokenType.MINUS);
        Node result = term();
        while (ops.contains(currentToken.getType())){
            Token token = currentToken;
            if (token.getType() == TokenType.PLUS){
                checkTokenType(TokenType.PLUS);
            }
            else if (token.getType() == TokenType.MINUS){
                checkTokenType(TokenType.MINUS);
            }
            result = new BinOp(result, token, term());
        }
        return result;
    }

    public Node parse() throws Exception {
        return expr();
    }


    public static void main(String[] args) throws Exception {
        Lexer lexer = new Lexer("2 + (2 * 3) * 4");
        Parser parser = new Parser(lexer);
        System.out.println(parser.parse());
    }

}
