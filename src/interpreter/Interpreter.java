package interpreter;

public class Interpreter implements NodeVisitor {
    private Parser parser;

    public Interpreter(Parser parser){
        this.parser = parser;
    }

    @Override
    public float visit(Node node) throws Exception {
        if (node.getClass().equals(BinOp.class)){
            return visitBinOp(node);
        }
        else if (node.getClass().equals(Number.class)){
            return visitNumber(node);
        }
        else if (node.getClass().equals(UnaryOp.class)){
            return visitUnaryOp(node);
        }
        throw new Exception("Interpreter error!");
    }

    public float visitBinOp(Node node) throws Exception {
        System.out.println("visit BinOp");
        BinOp binop = (BinOp) node;
        if (binop.getOp().getType().equals(TokenType.PLUS)){
            return visit(binop.getLeft()) + visit(binop.getRight());
        }
        else if (binop.getOp().getType().equals(TokenType.MINUS)){
            return visit(binop.getLeft()) - visit(binop.getRight());
        }
        else if (binop.getOp().getType().equals(TokenType.DIV)){
            return visit(binop.getLeft()) / visit(binop.getRight());
        }
        else if (binop.getOp().getType().equals(TokenType.MUL)){
            return visit(binop.getLeft()) * visit(binop.getRight());
        }
        throw new Exception("BinOp error");
    }

    public float visitNumber(Node node){
        System.out.println("visit Number");
        Number number = (Number) node;
        return Float.parseFloat(number.getToken().getValue());
    }

    public float visitUnaryOp(Node node) throws Exception {
        System.out.println("visit UnaryOp");
        UnaryOp op = (UnaryOp) node;
        if (op.getToken().getType().equals(TokenType.PLUS)){
            return +visit(op.getExpr());
        }
        else if (op.getToken().getType().equals(TokenType.MINUS)){
            return -visit(op.getExpr());
        }
        throw new Exception("UnaryOp error!");
    }

    public float interpret() throws Exception {
        Node tree = parser.parse();
        System.out.println(tree);
        return visit(tree);
    }

    public static void main(String[] args) throws Exception {
        Lexer lexer = new Lexer("-2 +-+--------- 2 * (2 + 2)");
        Parser parser = new Parser(lexer);
        Interpreter interpreter = new Interpreter(parser);
        System.out.println(interpreter.interpret());
    }


}
