package logging;

import java.util.ArrayList;

public class Logger {
    private static Logger instance;
    private ArrayList<BaseHandler> handlers = new ArrayList<>();

    private Logger(){

    }

    public static Logger getInstance(){
        if (instance == null){
            instance = new Logger();
        }
        return instance;
    }

    public void attach(BaseHandler handler){
        handlers.add(handler);
    }
    public void dettach(BaseHandler handler){
        handlers.remove(handler);
    }

    public void log(String message, Message.MType type){
        for (BaseHandler handler: handlers){
            handler.log(new Message(message, type));
        }
    }

    public void warn(String message){
        log(message, Message.MType.WARNING);
    }
    public void info(String message){
        log(message, Message.MType.INFO);
    }
    public void critical(String message){
        log(message, Message.MType.CRITICAL);
    }


    public static void main(String[] args){
        Logger.getInstance().attach(new ConsoleHandler(
                new StringFormatter()));
        Logger.getInstance().attach(new ConsoleHandler(
                new StringFormatter()));
        Logger.getInstance().warn("Warning message");
        Logger.getInstance().critical("Critical message");
    }

}
