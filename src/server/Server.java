package server;

import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;

public class Server {
    private ServerSocket serverSock;
    private Socket clientSock;
    private PrintWriter out;
    private BufferedReader in;

    public void start(int port) throws IOException, ParseException {
        serverSock = new ServerSocket(port);
        JSONParser parser = new JSONParser();

        while (true) {
            System.out.println("Wait for new connection...");
            ClientTask clientTask = new ClientTask(serverSock.accept()); //ждет пока не придет клиент
            System.out.println("Start new Thread");
            new Thread(clientTask).start();
        }
        //serverSock.close();
    }

    public static void main(String[] args) throws IOException, ParseException {
        Server server = new Server();
        server.start(5000);
    }
}
